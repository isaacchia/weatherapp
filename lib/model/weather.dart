
class WeatherInfo{

  int weatherId;
  String weatherName;
  String weatherIcon;
  double temperature;
  double minTemperature;
  double maxTemperature;
  DateTime dateTime;
  DateTime sunsetTime;
  DateTime sunriseTime;

  int placeId;
  String placeName;

  WeatherInfo(
      this.weatherId,
      this.weatherName,
      this.weatherIcon,
      this.temperature,
      this.minTemperature,
      this.maxTemperature,
      this.dateTime,
      this.sunsetTime,
      this.sunriseTime,
      this.placeId,
      this.placeName
  );
}