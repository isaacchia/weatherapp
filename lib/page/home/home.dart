import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:weather_app/base/page_base_class.dart';
import 'package:weather_app/component/default_text.dart';
import 'package:weather_app/model/weather.dart';
import 'package:weather_app/route_generator.dart';
import 'package:weather_app/utils/common.dart';
import 'package:weather_app/utils/constant.dart';

import 'home_viewmodel.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends PageBaseClass with WidgetsBindingObserver {
  var _viewModel = HomeViewModel();
  TabController _tabController;
  bool _firstLoad = true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    _viewModel.showErrorCommand.listen((message) {
      showErrorMessage(message);
    });
    _viewModel.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _viewModel.dispose();
    super.dispose();

  }

  @override
  void didChangeAppLifecycleState(final AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      _viewModel.setState(_viewModel.getState());
    }
  }

  void onTabChanged() {
    print("tab: ${_tabController.index.toString()}");
    int index = _tabController.index;
    if (index < _viewModel.getState().length){
      _viewModel.tabChangedCommand.execute(_viewModel.getState()[index]);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _background(),
          _body(),
          loadingView(_viewModel.showLoadingCommand)
        ],
      ),
    );
  }

  Widget _background() {
    return StreamBuilder(
        stream: _viewModel.tabChangedCommand,
        initialData: null,
        builder: (context, snapshot) {
          HomeState state = snapshot.data;
          if (state?.currentWeather != null){
            WeatherInfo weatherInfo = state.currentWeather;
            String backgroundImage = _viewModel.getBackgroundImage(weatherInfo);

            return Column(
              children: <Widget>[
                Expanded(
                       child: Container(
                           decoration: BoxDecoration(
                               image: DecorationImage(
                                   image: AssetImage(backgroundImage),
                                   fit: BoxFit.cover)
                           )
                       )
                ),
              ],
            );

          }else{
            return Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(ImagesConstant.backgroundCloudy),
                              fit: BoxFit.cover)
                      )
                  ),
                ),
              ],
            );
          }
        });



  }

  Widget _body() {
    return StreamBuilder(
        stream: _viewModel.fetchWeatherCommand,
        initialData: null,
        builder: (context, snapshot) {
          if (snapshot.data != null){

            _tabController = TabController(vsync: this, length: snapshot.data.length);
            _tabController.removeListener(onTabChanged);
            _tabController.addListener(onTabChanged);


            return pullToRefresh(
                onRefresh: () async {
                  _viewModel.setState(_viewModel.getState());
                },
                child: Column(
                  children: <Widget>[
                    _topBar(),
                    Expanded(
                      child: TabBarView(
                          controller: _tabController,
                          children: snapshot.data.map<Widget>((item) {
                            return _tabContent(item);
                          }).toList()),
                    ),
                  ],
                )
            );
          }else{
            return noDataView(_viewModel.fetchWeatherCommand,
                onTap: () => _viewModel.setState(_viewModel.getState()));
          }

        });


  }


  Widget _tabContent(HomeState state){

    if (_firstLoad){
      Future.delayed(Duration(milliseconds: 500), ()=>onTabChanged());
      _firstLoad = false;
    }

    return Stack(
      children: <Widget>[
        Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(child: _currentWeather(state)),
            _forecastWeather(state)
          ],
        ),
      ],
    );
  }

  Widget _topBar() {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0.0,
//      leading: Icon(Icons.location_on),
      actions: <Widget>[
        _currentLocationButton(),
        _cityButton()
      ],
      title: _locationName()
    );
  }

  Widget _currentLocationButton() {
    return Padding(
      padding: const EdgeInsets.only(right: 15),
      child: InkWell(
        child: Icon(Icons.my_location, color: Colors.white,),
        onTap: () {
          _viewModel.refreshCurrentLocation();
          _tabController.animateTo(0);
        },
      ),
    );
  }

  Widget _cityButton(){
    return Padding(
      padding: const EdgeInsets.only(right: 15),
      child: InkWell(
        child: Icon(Icons.location_city,  color: Colors.white),
        onTap: () async{
          var result  = await Navigator.pushNamed(context, RoutePath.USER_CITY);
          if (result != null && result is List<int>){
              _viewModel.updatePlaces(result);
              _tabController.animateTo(0);
          }
        },
      ),
    );


  }

  Widget _locationName(){
    return StreamBuilder(
        stream: _viewModel.tabChangedCommand,
        initialData: null,
        builder: (context, snapshot) {
          if (snapshot.data != null){
            HomeState state = snapshot.data;
            return Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "${state.currentWeather.placeName}",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  Text("Last updated: ${Common.formatDate(DateTime.now().toLocal(),  dateFormat:"yyyy-MM-dd HH:mm")}",
                    style: TextStyle(
                    fontSize: 10, color: Colors.white),
                  )
                ],
              ),
            );

          }else{
            print("tab null");
            return SizedBox.shrink();
          }
        });
  }

  Widget _currentWeather(HomeState state) {
    if (state.currentWeather != null){
      WeatherInfo weatherInfo = state.currentWeather;
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 80, horizontal: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "${weatherInfo.temperature.toInt()}°C",
              style: TextStyle(
                color: Colors.white,
                fontSize: 40,
              ),
            ),
            DefaultText("${weatherInfo.weatherName}  "
                "${weatherInfo.minTemperature.toInt()} / ${weatherInfo.maxTemperature.toInt()}°C")
          ],
        ),
      );

    }else{
      return SizedBox.shrink();
    }
  }

  Widget _forecastWeather(HomeState state) {

    if (state.forecastWeather != null && state.forecastWeather.isNotEmpty){
      List<WeatherInfo> weatherList = state.forecastWeather;
      return Container(
        height: 125,
        color: Colors.black.withOpacity(0.2),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
          child: ListView(

              scrollDirection: Axis.horizontal,
              children: weatherList.map((item){
                return _forecastItem(item);
              }).toList()
          ),
        ),
      );

    }else{

      return SizedBox.shrink();
    }


  }

  Widget _forecastItem(WeatherInfo weatherInfo) {

    var dayString = Common.formatDate(weatherInfo.dateTime, dateFormat: "EEEE");
    if (Common.checkIsSameDate(weatherInfo.dateTime, DateTime.now().toLocal())){
      dayString = "Today";
    }

    return Padding(
      padding: const EdgeInsets.only(right: 30),
      child: Column(
        children: <Widget>[
          DefaultText(dayString),
          DefaultText("${Common.formatDate(weatherInfo.dateTime, dateFormat: "HH:mm")}"),
          Image(
            height: 45,
            image: NetworkImage(
              weatherInfo.weatherIcon,
            )
          ),
          DefaultText("${weatherInfo.weatherName}")],
      ),
    );
  }
}
