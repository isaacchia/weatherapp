import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:rx_command/rx_command.dart';
import 'package:weather_app/base/viewmodel_base_class.dart';
import 'package:weather_app/data/repo/location_repo.dart';
import 'package:weather_app/data/repo/weather_repo.dart';
import 'package:weather_app/model/weather.dart';
import 'package:weather_app/utils/constant.dart';
import 'package:weather_app/utils/crash_report.dart';
import 'package:weather_app/utils/locator.dart';

class HomeViewModel with ViewModelBase {

  List<HomeState> _homeStates;
  RxCommand<bool, bool> showLoadingCommand;
  RxCommand<String, String> showErrorCommand;
  RxCommand<HomeState, HomeState> tabChangedCommand;
  RxCommand<List<HomeState>, List<HomeState>> fetchWeatherCommand;


  HomeViewModel(){

    _homeStates = new List<HomeState>();
    showLoadingCommand = RxCommand.createSync((b) => b);
    showErrorCommand = RxCommand.createSync((b) => b);
    tabChangedCommand = RxCommand.createSync((b) => b);

    fetchWeatherCommand =
        RxCommand.createAsync<List<HomeState>, List<HomeState>>(_fetchCurrentWeather);
    fetchWeatherCommand.isExecuting.listen((data) {
      showLoadingCommand.execute(data);
    });


  }

  getState(){
    return _homeStates;
  }

  setState(List<HomeState> newState){
    _homeStates = newState;
    fetchWeatherCommand.execute(_homeStates);
  }

  dispose() {
    fetchWeatherCommand.dispose();
    showErrorCommand.dispose();
    showLoadingCommand.dispose();
    tabChangedCommand.dispose();
  }

  initState() async{
    var savedPlaceIds = await Locator.getInstance<LocationRepo>().load();
    refreshCurrentLocation();
    _initPlace(savedPlaceIds);

  }

  _initPlace(List<int> placeIds) async{

    var tmpState = List<HomeState>();
    if (placeIds.isEmpty){
      var isInit = await Locator.getInstance<LocationRepo>().getIsInit();
      // set default if empty
      if (!isInit){
        placeIds = [1733046, 1735106, 1732752]; //KL, Georgetown, Johor Bahru
        await Locator.getInstance<LocationRepo>().save(placeIds);
        await Locator.getInstance<LocationRepo>().setIsInit(true);
      }
    }

    placeIds.forEach((item){
      tmpState.add(HomeState(item, null, null, false));
    });

    _homeStates.addAll(tmpState);

  }

  Future<List<HomeState>> _fetchCurrentWeather(List<HomeState> states) async{

    var errorMessage = "";
    print("fetch data");
    for (int i =0; i < states.length; i++ ){
      var currentWeatherResult = await Locator.getInstance<WeatherRepo>().getCurrentWeather(
          states[i].cityId,
          states[i].latitude,
          states[i].longitude);

      if (currentWeatherResult.data is WeatherInfo) {
        states[i].currentWeather = currentWeatherResult.data;
      }else{
        if (currentWeatherResult.message != null && currentWeatherResult.message.isNotEmpty){
          errorMessage = currentWeatherResult.message;
        }else {
          errorMessage = StringConstant.defaultAPIErrorMessage;
        }
      }

      var result = await Locator.getInstance<WeatherRepo>().getForecastWeather(
          states[i].cityId,
          states[i].latitude,
          states[i].longitude);

      if (result.data is List<WeatherInfo>) {
        states[i].forecastWeather = result.data;
      }else{
        if (currentWeatherResult.message != null && currentWeatherResult.message.isNotEmpty){
          errorMessage = currentWeatherResult.message;
        }else {
          errorMessage = StringConstant.defaultAPIErrorMessage;
        }
      }
    }

    if (errorMessage.isNotEmpty){
      showErrorCommand.execute(errorMessage);
    }

    return states;

  }

  void updatePlaces(List<int> cityIds) async{
    List<HomeState> tmpState = getState();
    tmpState.removeWhere((t)=>!t.isCurrentLocation);

    cityIds.forEach((item){
      tmpState.add(HomeState(item, null, null, false));
    });

    await Locator.getInstance<LocationRepo>().save(cityIds);
    setState(tmpState);
  }

  void refreshCurrentLocation() async {

    Map<PermissionGroup, PermissionStatus> permissions =
    await PermissionHandler()
        .requestPermissions([PermissionGroup.location]);

    if (permissions[PermissionGroup.location] == PermissionStatus.granted) {
      try {
        var currentLocation = await Geolocator()
            .getCurrentPosition(desiredAccuracy: LocationAccuracy.best);


        var newPlace = HomeState(
            0,
            currentLocation.latitude,
            currentLocation.longitude,
            true);

        _homeStates.removeWhere((t)=>t.isCurrentLocation);
        _homeStates.insert(0, newPlace);
        setState(_homeStates);

      } catch (exception, stackTrace) {
        CrashReport().logError(
            FlutterErrorDetails(exception: exception, stack: stackTrace));
        showErrorCommand.execute("Failed to detect current location. Please try again");
      }
    }
  }

  String getBackgroundImage(WeatherInfo weatherInfo){
    String backgroundImage = ImagesConstant.backgroundCloudy;
    int weatherId = weatherInfo.weatherId;

    if (weatherId < 300){
      backgroundImage = ImagesConstant.backgroundThunderstorm;
    }else if (weatherId < 600){
      backgroundImage = ImagesConstant.backgroundRaining;
    }else if (weatherId < 700){
      backgroundImage = ImagesConstant.backgroundSnow;
    } else if (weatherId == 800){
      if (DateTime.now().toLocal().compareTo( weatherInfo.sunsetTime) > 0){
        backgroundImage = ImagesConstant.backgroundNight;
      }else{
        backgroundImage = ImagesConstant.backgroundClear;
      }

    }else if (weatherId < 900){
      if (DateTime.now().toLocal().compareTo( weatherInfo.sunsetTime) > 0 || DateTime.now().toLocal().compareTo( weatherInfo.sunriseTime) < 0){
        backgroundImage = ImagesConstant.backgroundNight;
      }else{
        backgroundImage = ImagesConstant.backgroundCloudy;
      }

    }

    return backgroundImage;
  }
}


class HomeState{
  int cityId;
  double longitude;
  double latitude;
  bool isCurrentLocation;

  WeatherInfo currentWeather;
  List<WeatherInfo> forecastWeather = List<WeatherInfo>();

  HomeState(this.cityId, this.latitude, this.longitude, this.isCurrentLocation);
}


