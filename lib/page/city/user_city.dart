import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:weather_app/base/page_base_class.dart';
import 'package:weather_app/data/repo/location_repo.dart';
import 'package:weather_app/model/city.dart';
import 'package:weather_app/utils/constant.dart';
import 'package:weather_app/utils/locator.dart';

class UserCity extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _UserCityState();
  }
}

class _UserCityState extends PageBaseClass {
  List<CityInfo> userCities = List();

  @override
  void initState() {
    super.initState();
    _initData();
  }

  _initData() async {
    var data = await Locator.getInstance<LocationRepo>().getUserCities();
    setState(() {
      userCities = data;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
            iconTheme: IconThemeData(
              color: Colors.black, //change your color here
            ),
          backgroundColor: Colors.white,
          title: Text("Select city", style: TextStyle(color: Colors.black),)

        ),
        body: Stack(
          children: <Widget>[
            _body(),
          ],
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    var cityIds = userCities.where(((e)=>e.selected)).map((t) => t.id).toList();
    Navigator.pop(context, cityIds);
    return Future.value(false);
  }

  Widget _body() {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: ListView.separated(
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(userCities[index].name),
              trailing: userCities[index].selected ? Icon(Icons.done, color: Colors.green,): SizedBox.shrink(),
              onTap: (){
                userCities[index].selected =  !userCities[index].selected;
                setState(() {

                });
              },
            );
          },
          separatorBuilder: (context, index) => Divider(
                height: 20,
                color: Colors.grey[400],
              ),
          itemCount: userCities.length),
    );
  }
}
