import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:weather_app/utils/constant.dart';

class LoadingView extends StatelessWidget {
  final double opacity;
  final Color backgroundColor;
  final Color iconColors;
  final String displayText;
  final Color textColor;
  final double textSize;

  LoadingView(
      {Key key,
      this.displayText,
      this.opacity,
      this.backgroundColor,
      this.iconColors,
      this.textColor,
      this.textSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: true,
      child: Stack(
        children: <Widget>[
          Opacity(
            opacity: opacity ?? 1.0,
            child: Container(
              color: backgroundColor ?? Colors.black45,
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
//                Container(
//                    child: Image.asset(ImagesConstant.loadingAnimation, width: 180,),
//                decoration: BoxDecoration(
//                  color: Colors.white54,
//                  borderRadius: BorderRadius.circular(20)
//                ),),
              SpinKitDoubleBounce(color: iconColors ?? Colors.orange),
              SizedBox(
                height: 20,
              ),
              Text(
                displayText ?? "",
                style: TextStyle(
                  color: textColor ?? Colors.orange,
                  fontSize: textSize ?? 16,
                ),
              )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
