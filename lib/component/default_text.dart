import 'package:flutter/material.dart';

class DefaultText extends StatelessWidget {

  String data;
  TextStyle style;

  DefaultText(
      this.data, {
        Key key,
        this.style
      });

  @override
  Widget build(BuildContext context) {
    return Text(data, style: style ?? TextStyle(color: Colors.white,),);
  }


}