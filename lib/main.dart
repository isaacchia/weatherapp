import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:weather_app/page/home/home.dart';
import 'package:weather_app/route_generator.dart';
import 'package:weather_app/utils/constant.dart';
import 'package:weather_app/utils/locator.dart';

void main(){
  Locator.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));

    return MultiProvider(
      providers: [],
      child: MaterialApp(
        navigatorKey: Locator.getNavigator(),
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: ColorConstant.primaryColor,
          primarySwatch: Colors.blue,
        ),
        home: Scaffold(body: Home()),
        onGenerateRoute: RouteGenerator.generateRoute,
      ),
    );
  }

}
