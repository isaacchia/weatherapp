// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_client.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _ApiClient implements ApiClient {
  _ApiClient(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
  }

  final Dio _dio;

  String baseUrl;

  @override
  getForecastWeather(cityId, latitude, longitude, units) async {
    ArgumentError.checkNotNull(cityId, 'cityId');
    ArgumentError.checkNotNull(latitude, 'latitude');
    ArgumentError.checkNotNull(longitude, 'longitude');
    ArgumentError.checkNotNull(units, 'units');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{
      'id': cityId,
      'lat': latitude,
      'lon': longitude,
      'units': units
    };
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        'data/2.5/forecast',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ForecastWeatherResponse.fromJson(_result.data);
    return Future.value(value);
  }

  @override
  getCurrentWeather(cityId, latitude, longitude, units) async {
    ArgumentError.checkNotNull(cityId, 'cityId');
    ArgumentError.checkNotNull(latitude, 'latitude');
    ArgumentError.checkNotNull(longitude, 'longitude');
    ArgumentError.checkNotNull(units, 'units');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{
      'id': cityId,
      'lat': latitude,
      'lon': longitude,
      'units': units
    };
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        'data/2.5/weather',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = RealTimeWeatherResponse.fromJson(_result.data);
    return Future.value(value);
  }
}
