import 'package:retrofit/http.dart';
import 'package:dio/dio.dart';
import 'package:weather_app/data/model/forecast_weather_response.dart';
import 'package:weather_app/data/model/realtime_weather_response.dart';


part 'api_client.g.dart';

@RestApi()
abstract class ApiClient {
  factory ApiClient(Dio dio) = _ApiClient;

  @GET("data/2.5/forecast")
  Future<ForecastWeatherResponse> getForecastWeather(

      @Query("id") int cityId,
      @Query("lat") String latitude,
      @Query("lon") String longitude,
      @Query("units") String units);

  @GET("data/2.5/weather")
  Future<RealTimeWeatherResponse> getCurrentWeather(
      @Query("id") int cityId,
      @Query("lat") String latitude,
      @Query("lon") String longitude,
      @Query("units") String units);

//  @GET("api/books/checkCustomerCode/{customer_code}")
//  Future<bool> checkCustomerCode(@Path("customer_code") String customer_code);
//
//  // Register with customer code
//  @POST("api/books/postRegisterLoginEEP/en")
//  Future<RegisterWithCodeResponse> registerWithCode(@Body() var params);

//  @GET("https://maps.googleapis.com/maps/api/directions/json?mode=driving")
//  Future<MapDirectionResponse> getMapRoutes(
//      @Query("origin") String origin,
//      @Query("destination") String destination,
//      @Query("key") String key,
//      @Query("waypoints") String waypoints);
//
//  //@POST("core/login")
//  @POST("auth/login")
//  Future<LoginResponse> login(@Body() LoginRequest params);
//
//  @POST("auth/forget-password")
//  Future<ForgetPasswordResponse> forgetPassword(@Body() ForgetPasswordRequest param);
//
//
//  @POST("kpi/statistics")
//  Future<KPIDataResponse> getKPIData(@Body() KPIDataRequest param);
//
//  @POST("orders/statistics")
//  Future<DashboardDataResponse> getDashboardData(@Body() DashboardDataRequest param);
//
//  @POST("orders/details")
//  Future<OrderDetailListResponse> getOrderList(
//      @Body() OrderDetailListRequest params);
//
//  @GET("orders/details/{id}/tracking")
//  Future<OrderDetailResponse> getOrderDetail(@Path("id") String id);
//
//  @GET("orders/details/{id}/tracking/gps")
//  Future<OrderGPSResponse> getOrderDetailGPS(@Path("id") String id);
//
//  @GET("orders/options/projects")
//  Future<ProjectListResponse> getOrderProjectList(
//      @Query("startDate") String startDate, @Query("endDate") String endDate);
//
//  @GET("orders/options/projects/{project}/countries")
//  Future<CountryListResponse> getOrderCountryList(@Path("project") String project,
//      @Query("startDate") String startDate, @Query("endDate") String endDate);
//
//  @GET("orders/options/projects/{project}/countries/{country}/districts")
//  Future<DistrictListResponse> getOrderDistrictList(
//      @Path("project") String project,
//      @Path("country") int country,
//      @Query("startDate") String startDate,
//      @Query("endDate") String endDate);
//
//  @GET("kpi/options/projects")
//  Future<ProjectListResponse> getKPIProjectList(
//      @Query("range") String range); // range : WTD, MTD, YTD
//
//  @GET("kpi/options/projects/{project}/countries")
//  Future<CountryListResponse> getKPICountryList(@Path("project") String project,
//      @Query("range") String range); // range : WTD, MTD, YTD
//
//  @GET("/kpi/options/projects/{project}/countries/{country}/districts")
//  Future<DistrictListResponse> getKPIDistrictList(
//      @Path("project") String project,
//      @Path("country") int country,
//      @Query("range") String range); // range : WTD, MTD, YTD
}
