import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:weather_app/generated/i18n.dart';
import 'package:weather_app/route_generator.dart';
import 'package:weather_app/utils/app_config.dart';
import 'package:weather_app/utils/common.dart';
import 'package:weather_app/utils/locator.dart';
import 'dart:convert';
import 'api_client.dart';

class Api {
  static ApiClient getInstance() {
    return Locator.getInstance<ApiClient>();
  }

  static Dio getDio() {
    var dio = Dio();
    dio.options.baseUrl = AppConfig.getBaseUrl();
    dio.options.connectTimeout = AppConfig.connectTimeout();
    dio.options.receiveTimeout = AppConfig.receiveTimeout();
    dio.options.sendTimeout = AppConfig.sendTimeout();
//    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (HttpClient client) {
//      client.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
//      return client;
//    };

    dio.interceptors.add(
        LogInterceptor(responseBody: true, requestHeader: true, error: true));

    dio.interceptors.add(
      InterceptorsWrapper(
        onResponse: (Response response) {
          if (response.data != null && response.data is String) {
            response.data = jsonDecode(response.data);
          }
          return response;
        },
        onRequest: (RequestOptions options) async{

          if (options.method.toLowerCase() == "get"){
            options.queryParameters.addAll({"appid": AppConfig.getAppKey()});
          }

//          var token = await UserRepo().getAuthToken();
//          options.headers["Authorization"] = "Bearer $token";
//
//          if (options.data != null) {
//            Common.printWrapped('Request params: ' + jsonEncode(options.data));
//          }
          return options;
        },
        onError: (DioError error)async {
          print('error 401');
          // Assume 401 stands for token expired
          if(error.response?.statusCode==401 || error.response?.statusCode==403){

          }
          return error;
        },
      ),
    );



    return dio;
  }
}
