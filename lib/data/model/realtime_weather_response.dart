import 'package:json_annotation/json_annotation.dart';

part 'realtime_weather_response.g.dart';


@JsonSerializable()
class RealTimeWeatherResponse extends Object {

  @JsonKey(name: 'coord')
  Coord coord;

  @JsonKey(name: 'weather')
  List<Weather> weather;

  @JsonKey(name: 'base')
  String base;

  @JsonKey(name: 'main')
  Main main;

  @JsonKey(name: 'wind')
  Wind wind;

  @JsonKey(name: 'clouds')
  Clouds clouds;

  @JsonKey(name: 'dt')
  int dt;

  @JsonKey(name: 'sys')
  Sys sys;

  @JsonKey(name: 'timezone')
  int timezone;

  @JsonKey(name: 'id')
  int id;

  @JsonKey(name: 'name')
  String name;

  @JsonKey(name: 'cod')
  int cod;

  RealTimeWeatherResponse(this.coord,this.weather,this.base,this.main,this.wind,this.clouds,this.dt,this.sys,this.timezone,this.id,this.name,this.cod,);

  factory RealTimeWeatherResponse.fromJson(Map<String, dynamic> srcJson) => _$RealTimeWeatherResponseFromJson(srcJson);

  Map<String, dynamic> toJson() => _$RealTimeWeatherResponseToJson(this);

}


@JsonSerializable()
class Coord extends Object {

  @JsonKey(name: 'lon')
  double lon;

  @JsonKey(name: 'lat')
  double lat;

  Coord(this.lon,this.lat,);

  factory Coord.fromJson(Map<String, dynamic> srcJson) => _$CoordFromJson(srcJson);

  Map<String, dynamic> toJson() => _$CoordToJson(this);

}


@JsonSerializable()
class Weather extends Object {

  @JsonKey(name: 'id')
  int id;

  @JsonKey(name: 'main')
  String main;

  @JsonKey(name: 'description')
  String description;

  @JsonKey(name: 'icon')
  String icon;

  Weather(this.id,this.main,this.description,this.icon,);

  factory Weather.fromJson(Map<String, dynamic> srcJson) => _$WeatherFromJson(srcJson);

  Map<String, dynamic> toJson() => _$WeatherToJson(this);

}


@JsonSerializable()
class Main extends Object {

  @JsonKey(name: 'temp')
  double temp;

  @JsonKey(name: 'feels_like')
  double feelsLike;

  @JsonKey(name: 'temp_min')
  double tempMin;

  @JsonKey(name: 'temp_max')
  double tempMax;

  @JsonKey(name: 'pressure')
  int pressure;

  @JsonKey(name: 'humidity')
  int humidity;

  Main(this.temp,this.feelsLike,this.tempMin,this.tempMax,this.pressure,this.humidity,);

  factory Main.fromJson(Map<String, dynamic> srcJson) => _$MainFromJson(srcJson);

  Map<String, dynamic> toJson() => _$MainToJson(this);

}


@JsonSerializable()
class Wind extends Object {

  @JsonKey(name: 'speed')
  double speed;

  @JsonKey(name: 'deg')
  double deg;

  Wind(this.speed,this.deg,);

  factory Wind.fromJson(Map<String, dynamic> srcJson) => _$WindFromJson(srcJson);

  Map<String, dynamic> toJson() => _$WindToJson(this);

}


@JsonSerializable()
class Clouds extends Object {

  @JsonKey(name: 'all')
  int all;

  Clouds(this.all,);

  factory Clouds.fromJson(Map<String, dynamic> srcJson) => _$CloudsFromJson(srcJson);

  Map<String, dynamic> toJson() => _$CloudsToJson(this);

}


@JsonSerializable()
class Sys extends Object {

  @JsonKey(name: 'type')
  int type;

  @JsonKey(name: 'id')
  int id;

  @JsonKey(name: 'message')
  double message;

  @JsonKey(name: 'country')
  String country;

  @JsonKey(name: 'sunrise')
  int sunrise;

  @JsonKey(name: 'sunset')
  int sunset;

  Sys(this.type,this.id,this.message,this.country,this.sunrise,this.sunset,);

  factory Sys.fromJson(Map<String, dynamic> srcJson) => _$SysFromJson(srcJson);

  Map<String, dynamic> toJson() => _$SysToJson(this);

}


