import 'package:json_annotation/json_annotation.dart';

part 'city.g.dart';


@JsonSerializable()
class City extends Object {

  @JsonKey(name: 'city')
  String city;

  @JsonKey(name: 'admin')
  String admin;

  @JsonKey(name: 'country')
  String country;

  @JsonKey(name: 'population_proper')
  String populationProper;

  @JsonKey(name: 'iso2')
  String iso2;

  @JsonKey(name: 'capital')
  String capital;

  @JsonKey(name: 'lat')
  String lat;

  @JsonKey(name: 'lng')
  String lng;

  @JsonKey(name: 'population')
  String population;

  @JsonKey(name: 'id')
  int id;

  City(this.city,this.admin,this.country,this.populationProper,this.iso2,this.capital,this.lat,this.lng,this.population,this.id,);

  factory City.fromJson(Map<String, dynamic> srcJson) => _$CityFromJson(srcJson);

  Map<String, dynamic> toJson() => _$CityToJson(this);

}


