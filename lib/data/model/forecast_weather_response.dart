import 'package:json_annotation/json_annotation.dart';

part 'forecast_weather_response.g.dart';


@JsonSerializable()
class ForecastWeatherResponse extends Object {

  @JsonKey(name: 'cod')
  String cod;

  @JsonKey(name: 'message')
  int message;

  @JsonKey(name: 'cnt')
  int cnt;

  @JsonKey(name: 'list')
  List<ListObject> list;

  @JsonKey(name: 'city')
  City city;

  ForecastWeatherResponse(this.cod,this.message,this.cnt,this.list,this.city,);

  factory ForecastWeatherResponse.fromJson(Map<String, dynamic> srcJson) => _$ForecastWeatherResponseFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ForecastWeatherResponseToJson(this);

}


@JsonSerializable()
class ListObject extends Object {

  @JsonKey(name: 'dt')
  int dt;

  @JsonKey(name: 'main')
  Main main;

  @JsonKey(name: 'weather')
  List<Weather> weather;

  @JsonKey(name: 'clouds')
  Clouds clouds;

  @JsonKey(name: 'wind')
  Wind wind;

  @JsonKey(name: 'sys')
  Sys sys;

  @JsonKey(name: 'dt_txt')
  String dtTxt;

  ListObject(this.dt,this.main,this.weather,this.clouds,this.wind,this.sys,this.dtTxt,);

  factory ListObject.fromJson(Map<String, dynamic> srcJson) => _$ListObjectFromJson(srcJson);

  Map<String, dynamic> toJson() => _$ListObjectToJson(this);

}


@JsonSerializable()
class Main extends Object {

  @JsonKey(name: 'temp')
  double temp;

  @JsonKey(name: 'feels_like')
  double feelsLike;

  @JsonKey(name: 'temp_min')
  double tempMin;

  @JsonKey(name: 'temp_max')
  double tempMax;

  @JsonKey(name: 'pressure')
  int pressure;

  @JsonKey(name: 'sea_level')
  int seaLevel;

  @JsonKey(name: 'grnd_level')
  int grndLevel;

  @JsonKey(name: 'humidity')
  int humidity;

  @JsonKey(name: 'temp_kf')
  double tempKf;

  Main(this.temp,this.feelsLike,this.tempMin,this.tempMax,this.pressure,this.seaLevel,this.grndLevel,this.humidity,this.tempKf,);

  factory Main.fromJson(Map<String, dynamic> srcJson) => _$MainFromJson(srcJson);

  Map<String, dynamic> toJson() => _$MainToJson(this);

}


@JsonSerializable()
class Weather extends Object {

  @JsonKey(name: 'id')
  int id;

  @JsonKey(name: 'main')
  String main;

  @JsonKey(name: 'description')
  String description;

  @JsonKey(name: 'icon')
  String icon;

  Weather(this.id,this.main,this.description,this.icon,);

  factory Weather.fromJson(Map<String, dynamic> srcJson) => _$WeatherFromJson(srcJson);

  Map<String, dynamic> toJson() => _$WeatherToJson(this);

}


@JsonSerializable()
class Clouds extends Object {

  @JsonKey(name: 'all')
  int all;

  Clouds(this.all,);

  factory Clouds.fromJson(Map<String, dynamic> srcJson) => _$CloudsFromJson(srcJson);

  Map<String, dynamic> toJson() => _$CloudsToJson(this);

}


@JsonSerializable()
class Wind extends Object {

  @JsonKey(name: 'speed')
  double speed;

  @JsonKey(name: 'deg')
  int deg;

  Wind(this.speed,this.deg,);

  factory Wind.fromJson(Map<String, dynamic> srcJson) => _$WindFromJson(srcJson);

  Map<String, dynamic> toJson() => _$WindToJson(this);

}


@JsonSerializable()
class Sys extends Object {

  @JsonKey(name: 'pod')
  String pod;

  Sys(this.pod,);

  factory Sys.fromJson(Map<String, dynamic> srcJson) => _$SysFromJson(srcJson);

  Map<String, dynamic> toJson() => _$SysToJson(this);

}


@JsonSerializable()
class City extends Object {

  @JsonKey(name: 'id')
  int id;

  @JsonKey(name: 'name')
  String name;

  @JsonKey(name: 'coord')
  Coord coord;

  @JsonKey(name: 'country')
  String country;

  @JsonKey(name: 'timezone')
  int timezone;

  @JsonKey(name: 'sunrise')
  int sunrise;

  @JsonKey(name: 'sunset')
  int sunset;

  City(this.id,this.name,this.coord,this.country,this.timezone,this.sunrise,this.sunset,);

  factory City.fromJson(Map<String, dynamic> srcJson) => _$CityFromJson(srcJson);

  Map<String, dynamic> toJson() => _$CityToJson(this);

}


@JsonSerializable()
class Coord extends Object {

  @JsonKey(name: 'lat')
  double lat;

  @JsonKey(name: 'lon')
  double lon;

  Coord(this.lat,this.lon,);

  factory Coord.fromJson(Map<String, dynamic> srcJson) => _$CoordFromJson(srcJson);

  Map<String, dynamic> toJson() => _$CoordToJson(this);

}


