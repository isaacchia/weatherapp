// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'city.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

City _$CityFromJson(Map<String, dynamic> json) {
  return City(
    json['city'] as String,
    json['admin'] as String,
    json['country'] as String,
    json['population_proper'] as String,
    json['iso2'] as String,
    json['capital'] as String,
    json['lat'] as String,
    json['lng'] as String,
    json['population'] as String,
    json['id'] as int,
  );
}

Map<String, dynamic> _$CityToJson(City instance) => <String, dynamic>{
      'city': instance.city,
      'admin': instance.admin,
      'country': instance.country,
      'population_proper': instance.populationProper,
      'iso2': instance.iso2,
      'capital': instance.capital,
      'lat': instance.lat,
      'lng': instance.lng,
      'population': instance.population,
      'id': instance.id,
    };
