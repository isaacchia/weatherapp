import 'package:weather_app/data/api/api.dart';
import 'package:weather_app/model/result.dart';
import 'package:weather_app/model/weather.dart';
import 'package:weather_app/utils/app_config.dart';
import 'package:weather_app/utils/common.dart';

import 'base_repo.dart';
import 'weather_repo.dart';

class WeatherRepoImpl extends BaseRepo implements WeatherRepo{

  Future<Result> getCurrentWeather(int cityId, double latitude, double longitude) async{


    try {
      String units = AppConfig.getTemperatureUnit();
      var response = await Api.getInstance()
          .getCurrentWeather(cityId, latitude?.toString() ?? "", longitude?.toString() ?? "", units);

      if (response.cod == 200 &&
          response.weather != null &&
          response.weather.isNotEmpty &&
          response.main != null &&
          response.dt != null) {

        var weatherObj = response.weather[0];
        var weatherInfo = WeatherInfo(
            weatherObj.id,
            weatherObj.main,
            weatherObj.icon,
            response.main.temp,
            response.main.tempMin,
            response.main.tempMax,
            DateTime.fromMillisecondsSinceEpoch(response.dt * 1000, isUtc: true).toLocal(),
            DateTime.fromMillisecondsSinceEpoch(response.sys.sunset * 1000, isUtc: true).toLocal(),
            DateTime.fromMillisecondsSinceEpoch(response.sys.sunrise * 1000, isUtc: true).toLocal(),
            response.id,
            response.name
        );

        return Result(true, data: weatherInfo);
      } else {
        return Result(false, message: "");
      }
    } catch (exception, stackTrace) {
      return handleError(exception, stackTrace);
    }
  }

  Future<Result> getForecastWeather(int cityId, double latitude, double longitude) async{

    try {
      String units = AppConfig.getTemperatureUnit();
      var response = await Api.getInstance()
          .getForecastWeather(cityId, latitude?.toString() ?? "", longitude?.toString() ?? "", units);


      if (response.cod == "200" &&
          response.list != null &&
          response.list.isNotEmpty) {

        var resultList = List<WeatherInfo>();

        response.list.forEach((item){
          if (item.weather != null && item.main != null){
            var weatherObj = item;
            var iconUrl = "${AppConfig.getWeatherIconBaseUrl()}${weatherObj.weather.first.icon}@2x.png";
            var dateTime =  DateTime.fromMillisecondsSinceEpoch(weatherObj.dt * 1000, isUtc: true).toLocal();
            var sunsetDateTime =  DateTime.fromMillisecondsSinceEpoch(response.city.sunset * 1000, isUtc: true).toLocal();
            var sunriseDateTime =  DateTime.fromMillisecondsSinceEpoch(response.city.sunrise * 1000, isUtc: true).toLocal();

            if (Common.checkIsLaterDateTime(dateTime)){
              var weatherInfo = WeatherInfo(
                  weatherObj.weather.first?.id,
                  weatherObj.weather.first?.main,
                  iconUrl,
                  weatherObj.main.temp,
                  weatherObj.main.tempMin,
                  weatherObj.main.tempMax,
                  dateTime,
                  sunsetDateTime,
                  sunriseDateTime,
                  response.city?.id,
                  response.city?.name
              );

              resultList.add(weatherInfo);
            }


          }

        });

        return Result(true, data: resultList);
      } else {
        return Result(false, message: "");
      }
    } catch (exception, stackTrace) {
      return handleError(exception, stackTrace);
    }
  }

}