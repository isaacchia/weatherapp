import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:weather_app/data/model/city.dart';
import 'package:weather_app/model/city.dart';
import 'package:weather_app/utils/preference.dart';
import 'location_repo.dart';

class LocationRepoImpl implements LocationRepo{

  final String KEY_PLACE_ID = "KEY_PLACE_ID";
  final String KEY_PLACE_IS_INIT = "KEY_PLACE_IS_INIT";


  Future save(List<int> placeIds) async{
    await Preference.setStringList(KEY_PLACE_ID, placeIds.map((t)=>t.toString()).toList());
  }

  Future<List<int>> load() async{
    var values = await Preference.getStringList(KEY_PLACE_ID, def: List<String>());
    if (values.isNotEmpty){
      return values.map((t)=> int.parse(t)).toList();
    }else{
      return List<int>();
    }
  }

  Future setIsInit(bool isInit) async{
    await Preference.setBool(KEY_PLACE_IS_INIT, isInit);
  }

  Future<bool> getIsInit() async{
    return await Preference.getBool(KEY_PLACE_IS_INIT, def: false);
  }


  Future<List<City>> _getCityList()async{
    var list = await _parseJsonFile('assets/data/city.json');
    return list;
  }

  Future<List<CityInfo>> getUserCities() async{
    var list = await _getCityList();
    var userCitiesIds = await load();

    return list.map((item){
      var selected = userCitiesIds.contains(item.id);
      return CityInfo(item.id, item.city, selected);
    }).toList();
  }

  static Future<dynamic> _parseJsonFile(String file) async {
    final dynamic jsonStr = await rootBundle.loadString(file);
    List value = json.decode(jsonStr);
    return value.map((t)=> City.fromJson(t)).toList();
  }
}