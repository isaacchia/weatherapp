
abstract class WeatherRepo {
  getCurrentWeather(int cityId, double latitude, double longitude);
  getForecastWeather(int cityId, double latitude, double longitude);
}