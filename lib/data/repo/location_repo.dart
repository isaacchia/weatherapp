

import 'package:weather_app/data/model/city.dart';
import 'package:weather_app/model/city.dart';

abstract class LocationRepo{

  Future save(List<int> placeIds);
  Future<List<int>> load();
  Future setIsInit(bool isInit);
  Future<bool> getIsInit();
  Future<List<CityInfo>> getUserCities();
}