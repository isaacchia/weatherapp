import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:weather_app/model/result.dart';
import 'package:weather_app/utils/crash_report.dart';


class BaseRepo {
  Result handleError(exception, stackTrace) {
    CrashReport().logError(
      FlutterErrorDetails(exception: exception, stack: stackTrace),
    );

    String errorMsg = "";
    if (exception is DioError) {
      /* errorMsg = exception.response?.statusCode == null
          ? exception.message
          : "HTTP Error: ${exception.response?.statusCode}"; */

      switch (exception.type) {
        case DioErrorType.CONNECT_TIMEOUT:
          errorMsg =
              'Unable to reach the server. Please verify your internet connection and try again.';
          break;
        case DioErrorType.RECEIVE_TIMEOUT:
          errorMsg =
              'Failed to receive a response. Please verify your internet connection and try again.';
          break;
        case DioErrorType.SEND_TIMEOUT:
          errorMsg =
              'Failed to send a request. Please verify your internet connection and try again.';
          break;
        case DioErrorType.RESPONSE:
          if (exception?.response?.data["message"] != null){
            errorMsg = exception?.response?.data["message"];
          }else{
            errorMsg = exception.message ?? "HTTP Error ${exception.response?.statusCode}";
          }
          break;
        case DioErrorType.CANCEL:
          if (exception?.response?.data["message"] != null){
            errorMsg = exception?.response?.data["message"];
          }else{
            errorMsg = exception.message ?? "HTTP Error ${exception.response?.statusCode}";
          }
          break;
        case DioErrorType.DEFAULT:
//          errorMsg =
//              'Cannot retrieve data at this time. Please try again later.';
          break;
        default:
//          errorMsg =
//              'Cannot retrieve data at this time. Please try again later.';
          break;
      }
    } else {
      errorMsg = exception?.toString();
    }
    return Result(false, message: errorMsg);
  }
}
