import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:weather_app/page/city/user_city.dart';
import 'package:weather_app/page/home/home.dart';
import 'package:weather_app/utils/animation_transition.dart';

class RoutePath{
  static const String HOME = '/';
  static const String USER_CITY = '/user_city';
  static const String ADD_CITY = '/add_city';
}

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    var data = settings.arguments;

    switch (settings.name) {
      case RoutePath.HOME:
        return MaterialPageRoute(
          settings: RouteSettings(name: RoutePath.HOME),
          builder: (_) => Home(),
        );

      case RoutePath.USER_CITY:
        return PageRouteBuilder(
          opaque: false,
          pageBuilder: (_, __, ___) => UserCity(),
          transitionsBuilder: (_, anim, secondAnim, child) {
            return AnimationTransition.fromRight(anim, secondAnim, child);
          },
        );


      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Error'),
          ),
          body: Center(
            child: Text('ERROR'),
          ),
        );
      },
    );
  }
}
