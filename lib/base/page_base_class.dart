import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:weather_app/component/custom_snackbar.dart';
import 'package:weather_app/component/loading_view.dart';
import 'package:weather_app/utils/constant.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:rx_command/rx_command.dart';

class PageBaseClass<T extends StatefulWidget> extends State<T>
    with TickerProviderStateMixin {
  final RefreshController _refreshController = RefreshController();
  AnimationController _scaleController;

  @override
  Widget build(BuildContext context) {
    return build(context);
  }

  @override
  void initState() {
    print('mixin');
    _scaleController =
        AnimationController(value: 0.0, vsync: this, upperBound: 1.0);
    _refreshController.headerMode.addListener(() {
      if (_refreshController.headerStatus == RefreshStatus.idle) {
        _scaleController.value = 0.0;
      } else if (_refreshController.headerStatus == RefreshStatus.refreshing) {}
    });
    super.initState();
  }

  void fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  void showErrorMessage(String message) {
    CustomSnackbar().show(context, message, MessageType.ERROR);
  }

  Widget loadingView(Stream stream) {
    return StreamBuilder<bool>(
        stream: stream,
        builder: (BuildContext context, AsyncSnapshot<bool> isRunning) {
          if (isRunning.data == true) {
            return LoadingView();
          } else {
            return SizedBox();
          }
        });
  }

  Widget pullToRefresh({child: Widget, Function onRefresh}) {
    return SmartRefresher(
      controller: _refreshController,
      enablePullDown: true,
      onRefresh: () async {
//        await Future.delayed(Duration(seconds: 4));
        if (onRefresh != null) {
          await onRefresh();
        }
        _refreshController.refreshCompleted();
      },
      child: child,
      header: CustomHeader(
        refreshStyle: RefreshStyle.Behind,
        onOffsetChange: (offset) {
          if (_refreshController.headerMode.value != RefreshStatus.refreshing)
            _scaleController.value = offset / 80.0;
        },
        builder: (c, m) {
          return Container(
            child: FadeTransition(
              opacity: _scaleController,
              child: ScaleTransition(
                child: SpinKitFadingCircle(
                  size: 30.0,
                  duration: Duration(milliseconds: 2000),
                  itemBuilder: (_, int index) {
                    return DecoratedBox(
                      decoration: BoxDecoration(
                        color: index.isEven ? Colors.orange : Colors.green,
                      ),
                    );
                  },
                ),
                scale: _scaleController,
              ),
            ),
            alignment: Alignment.center,
          );
        },
      ),
    );
  }

  Widget noDataView(RxCommand retrieveDataCommand, {Function onTap}) {
    return InkWell(
      onTap: () {
    if (onTap != null){
      onTap();
    }else{
      retrieveDataCommand.execute();
    }
      },
      child: StreamBuilder(
      stream: retrieveDataCommand.isExecuting,
      builder: (context, snapshot) {
        if (snapshot?.data == null || snapshot.data) {
          return SizedBox.shrink();
        } else {
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
//                  Image.asset(
//                    ImagesConstant.noDataFound,
//                    width: 150,
//                  ),
                  Text(
                    "Sorry, no result found",
                    style: TextStyle(
                        color: ColorConstant.textGrey,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Tap to refresh",
                    style: TextStyle(color: Colors.grey[600], fontSize: 15),
                  )
                ],
              ),
            ],
          );
        }
      }),
    );
  }
}
