import 'package:weather_app/model/result.dart';
import 'package:rxdart/rxdart.dart';

mixin ViewModelBase {
  executeWithLoading(BehaviorSubject<bool> subject, Future<Result> func) async {
    subject.sink.add(true);
    var result = await func;
    subject.sink.add(false);
    return result;
  }
}
