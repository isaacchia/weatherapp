import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class ColorConstant {
  static const primaryColor = Color(0xffFF7100);
  static const green = Colors.green;
  static const red = Colors.red;
  static const amberAccent = Colors.amberAccent;
  static const btnColor = Color(0xfff29f18);
  static const black1 = const Color(0xF0161616);
  static const black2 = const Color(0xff0D0D0D);
  static const black3 =  const Color(0xff1D1D1D);

  static const contentBackground = const Color(0xff202020);
  static const bottomBarColor = const Color(0xff313131);
  static const textInactiveColor = const Color(0x75757599);
  static const textActiveColor = Colors.white;
  static const textGrey = const Color(0xffB1B1B1);

  static const contentDark = const Color(0xff313131);
  static const backgroundDark = const Color(0xff202020);
}

class ImagesConstant {
  static String noDataFound = 'assets/images/no_data_found.png';
  static String backgroundCloudy = 'assets/images/background_cloudy.jpg';
  static String backgroundThunderstorm = 'assets/images/background_thunderstorm.jpg';
  static String backgroundRaining = 'assets/images/background_rain.jpg';
  static String backgroundSnow = 'assets/images/background_snow.jpg';
  static String backgroundAtmosphere  = 'assets/images/background_cloudy.jpg';
  static String backgroundClear  = 'assets/images/background_clear.jpg';
  static String backgroundNight  = 'assets/images/background_night_cloud.jpg';
}


class StringConstant{
  static const defaultAPIErrorMessage = "Some error occurred. Please try again.";
}


class KeyConstant{
  static const LOGIN_TEXT_PHONE_EMAIL = "LOGIN_TEXT_PHONE_EMAIL";
  static const LOGIN_TEXT_PASSWORD = "LOGIN_TEXT_PASSWORD";
  static const LOGIN_BUTTON_SUBMIT = "LOGIN_BUTTON_SUBMIT";
  static const SNACKBAR = "SNACKBAR_KEY";
  static const SNACKBAR_TEXT = "SNACKBAR_TEXT_KEY";
}
