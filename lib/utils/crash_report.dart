import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';

class CrashReport{

  void logError(FlutterErrorDetails details) {
    //Crashlytics.instance.enableInDevMode = true;
    // Pass all uncaught errors to Crashlytics.
    if (details != null){
      if (details.stack != null){
        logMessage(details.exception);
        //Crashlytics.instance.onError(details);
      }else{
        logMessage(details.exception);
      }
    }
  }

  void setUserIdentifier(String userIdentifier){
    Crashlytics.instance.setUserIdentifier(userIdentifier);
  }

  void setUserEmail(String email){
    Crashlytics.instance.setUserEmail(email);
  }

  void setUserPhone(String phone){
    Crashlytics.instance.setUserName(phone);
  }

  void logMessage(String msg){
    throw Exception(msg);
  }

}