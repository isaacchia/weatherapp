import 'dart:convert';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:package_info/package_info.dart';

class Common{

  Future<String> getDeviceIdentifier(BuildContext context) async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  static Future<String> getAppVersion() async{
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return "v${packageInfo.version}";
  }

  static void printWrapped(String text) {
    final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
    pattern.allMatches(text).forEach((match) => print(match.group(0)));
  }

  static String formatCurrency(double amount){
    final formatter = NumberFormat('#,###');
    return formatter.format(amount.toDouble()).toString();
  }

  static String colorToHex(Color color){
    var hex = '#${color.value.toRadixString(16)}';
    return hex;
  }

  static String formatDate(DateTime dateTime, {String dateFormat}) {
    return DateFormat(dateFormat ?? "yyyy-MM-dd").format(dateTime);
  }

  static bool checkIsSameDate(DateTime date1, DateTime date2){
    return date1.year == date2.year && date1.month == date2.month
        && date1.day == date2.day;
  }

  static bool checkIsLaterDateTime(DateTime newDate){
    DateTime dateNow = DateTime.now().toLocal();
    return newDate.year >= dateNow.year && newDate.month >= dateNow.month
        && newDate.day >= dateNow.day && newDate.hour >= dateNow.hour;
  }

  static String capitalizeFirstLetter(String s) => s[0].toUpperCase() + s.substring(1);

  static double kelvinToCelcius(double temparature){
    return temparature - 273.15;
  }

  static double kelvinToFahrenheit(double temparature){
    return ((temparature - 273.15) * 9/5) + 32;
  }

  static Future<dynamic> parseJsonFile(String file) async {
    final dynamic jsonStr = await rootBundle.loadString(file);
    dynamic valueMap = json.decode(jsonStr);
    return valueMap;
  }

}