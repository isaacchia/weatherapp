import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:weather_app/data/api/api.dart';
import 'package:weather_app/data/api/api_client.dart';
import 'package:weather_app/data/repo/location_repo.dart';
import 'package:weather_app/data/repo/location_repo_impl.dart';
import 'package:weather_app/data/repo/weather_repo.dart';
import 'package:weather_app/data/repo/weather_repo_impl.dart';



class Locator {

  static final GlobalKey<NavigatorState> navigator =  new GlobalKey<NavigatorState>();

  static T getInstance<T>(){
    return GetIt.instance<T>();
  }

  static void init(){
    GetIt getIt = GetIt.instance;
    getIt.registerSingleton<WeatherRepo>(WeatherRepoImpl());
    getIt.registerSingleton<LocationRepo>(LocationRepoImpl());
    getIt.registerSingleton<ApiClient>(ApiClient(Api.getDio()));
  }

  static GlobalKey<NavigatorState> getNavigator(){
    return navigator;
  }
}