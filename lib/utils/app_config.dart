class AppConfig {

  static FLAVOR _appFlavor;
  static final String _appKey = "656b487736a456851c60eb6eaff1b71c";

  static void setAppFlavor(FLAVOR value){
    _appFlavor = value;
  }

  static FLAVOR getAppFlavor(){
    return _appFlavor;
  }

  static String getBaseUrl() {
    if (_appFlavor == FLAVOR.uat){
      return "https://api.openweathermap.org/";
    }else if (_appFlavor == FLAVOR.prod){
      return "https://api.openweathermap.org/";
    }else{ // dev
      return "https://api.openweathermap.org/";
    }
  }

  static String getWeatherIconBaseUrl() {
    return "https://openweathermap.org/img/wn/";
  }

  static String getAppKey(){
    return _appKey;
  }

  static String getTemperatureUnit(){
    return "metric";
  }

  static int connectTimeout() {
    return 10000;
  }

  static int receiveTimeout() {
    return 15000;
  }

  static int sendTimeout() {
    return 15000;
  }
}


enum FLAVOR{
  dev,
  uat,
  prod
}
